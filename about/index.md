---
layout: layouts/post.njk
title: About Me
templateClass: tmpl-post
date: git Last Modified
eleventyNavigation:
    key: About
    order: 4
---

Hello, my name is *Pascal* (he/him), but you can just call me *spezifisch*.

**Contact:** You can reach me via email at [spezifisch+es@gmail.com](mailto:spezifisch+es@gmail.com).

**Encryption:** You can encrypt your messages to me using PGP: Key ID `AD5370AF218164CE`, download the public key [spezifisch.asc from here](https://spezifisch.codeberg.page/spezifisch.asc) or [from pgp.mit.edu](https://pgp.mit.edu/pks/lookup?op=vindex&search=0xAD5370AF218164CE). Please ensure the imported fingerprint is `308F 3B59 5B24 A5AC F5BC  826A AD53 70AF 2181 64CE`.

**Pointers:**

* Feel free to point out any mistakes in my blog posts.
* If my code is wrong, just open an issue in the respective repository.
* I begrudgingly publish code on [GitHub](https://github.com/spezifisch), and happily on [Codeberg](https://codeberg.org/spezifisch>).

## Credits

Sources of these blog pages can be found at: <https://codeberg.org/spezifisch/spezifisch-pages>.
Page generation code is published under MIT license.
Posts, images, and logo are "all rights reserved". Of course you may cite me within [fair use](https://guides.library.harvard.edu/gsd/write/permissions).

3rd party credits:

* based on <https://github.com/11ty/eleventy-base-blog>, [MIT license](https://github.com/11ty/eleventy-base-blog/blob/main/LICENSE), by Zach Leatherman @zachleat
* design parts taken from <https://github.com/TigersWay/fruits-express>, [MIT license](https://github.com/TigersWay/fruits-express/blob/main/LICENSE), by Ben Michaud (<https://tigersway.net> & <https://github.com/TigersWay>)
* additional design parts taken from <https://github.com/11ty/11ty-website>, [MIT license](https://github.com/11ty/11ty-website/blob/main/LICENSE), by Zach Leatherman
* header/footer images: [Saft Vektor erstellt von macrovector - de.freepik.com](https://de.freepik.com/vektoren-kostenlos/saftspray-und-fruchtillustration_13822592.htm)
* dark mode header image is just a (client-side) CSS filter on top
* *Main font*: [Roboto](https://fonts.google.com/specimen/Roboto/about), [Apache 2.0 license](https://www.apache.org/licenses/LICENSE-2.0)
* `Code font`: [Hack](https://github.com/source-foundry/Hack), [MIT license](https://github.com/source-foundry/Hack/blob/master/LICENSE.md)
* external link icon: [heroicons](https://heroicons.com/), [MIT license](https://github.com/tailwindlabs/heroicons/blob/master/LICENSE)
