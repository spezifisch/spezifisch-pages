const fs = require("fs");
const path = require("path");

const { DateTime } = require("luxon");
const markdownIt = require("markdown-it");
const markdownItAnchor = require("markdown-it-anchor");
const markdownItAttrs = require("markdown-it-attrs");
const markdownItFootnote = require("markdown-it-footnote");
const markdownItForInline = require("markdown-it-for-inline");

const pluginRss = require("@11ty/eleventy-plugin-rss");
const pluginSyntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");
const pluginNavigation = require("@11ty/eleventy-navigation");
const pluginImage = require("@11ty/eleventy-img");

// based on: https://www.11ty.dev/docs/plugins/image/
// examples:
// {% image src="./src/images/cat.jpg", caption="photo of my cat" %}
// {% image src="./src/images/cat.jpg", caption="photo of my cat", sizes="(min-width: 30em) 50vw, 100vw" %}
// other parameters:
// - border=false => don't show drop shadow in light mode
// - darker=true => caption is highlighted, background is darker in light mode; brighter in dark mode
// - link=false => don't link image to open in full size
async function imageShortcode({
    src,
    alt,
    sizes,
    caption,
    border,
    background,
    darker,
    link,
}) {
    const options = {
        // keep original size
        widths: [null],
        // keep original format
        formats: [null],
        // keep own images separate from theme images
        urlPath: "/assets/img/",
        // copy directly to output dir
        outputDir: "./_site/assets/img/",
        filenameFormat: function (id, src, width, format, _options) {
            const extension = path.extname(src);
            const name = path.basename(src, extension);

            return `${name}-${id}-orig.${format}`;
        },
    };
    const metadata = await pluginImage(src, options);

    let imageAttributes = {
        alt,
        sizes,
        decoding: "async",
    };

    // set classes with or without border
    let classes = "post-card";
    if (border !== false) {
        classes += " post-card-shadow";
    }
    if (darker) {
        classes += " post-card-darker";
    }
    // background
    if (background === "white") {
        classes += " post-card-bgwhite";
    }

    let imageHTML = pluginImage.generateHTML(metadata, imageAttributes);

    // add a link to see the image in original size
    if (link !== false) {
        let imgSrc;

        // metadata contains keys like png, jpeg, ... for every format that was generated
        for (const key in metadata) {
            // format is a list of image metadata (possibly multiple with different resolutions)
            const format = metadata[key];

            // find the biggest image
            let biggest_resolution = 0;
            let biggest;
            for (const item of format) {
                const res = item.width * item.height;
                if (!biggest_resolution || res > biggest_resolution) {
                    biggest_resolution = res;
                    biggest = item;
                }
            }

            if (biggest_resolution && biggest) {
                // found one. good enough for me for now.
                imgSrc = biggest.url;
                break;
            }
        }

        // link to original image if the hack above worked:
        if (imgSrc) {
            imageHTML = `<a href="${imgSrc}" title="Open image" target="_blank">${imageHTML}</a>`;
        }
    }

    if (caption === undefined) {
        // no caption added
        return `<figure class="${classes}">${imageHTML}</figure>`;
    }

    // add caption
    return `<figure class="${classes}">${imageHTML}<figcaption>${caption}</figcaption></figure>`;
}

module.exports = function (eleventyConfig) {
    const pathPrefix = "/";

    // Copy the `img` and `css` folders to the output
    eleventyConfig.addPassthroughCopy({ "style/img": "img" }); // style images
    eleventyConfig.addPassthroughCopy({ "style/css": "css" });
    eleventyConfig.addPassthroughCopy({
        // special locations
        "style/img/favicon.ico": "favicon.ico",
        "misc/robots.txt": "robots.txt",
        "misc/spezifisch.asc": "spezifisch.asc",
    });
    eleventyConfig.addPassthroughCopy({ "style/fonts": "assets/fonts" }); // style fonts
    eleventyConfig.addPassthroughCopy({ "posts/assets": "assets/posts" }); // posts extra files

    // image resize/formatting plugin
    eleventyConfig.addNunjucksAsyncShortcode("image", imageShortcode);

    // nicer dashes
    eleventyConfig.addTransform("replaceDashes", async function (content) {
        // this one is too trigger happy for me, e.g. in shell code `foo -- bar`
        //content = content.replace(/ -- /g, " – "); // en dash

        content = content.replace(/ --- /g, " — "); // em dash
        return content;
    });

    // Add plugins
    eleventyConfig.addPlugin(pluginRss);
    eleventyConfig.addPlugin(pluginSyntaxHighlight);
    eleventyConfig.addPlugin(pluginNavigation);

    // date part for permalinks
    eleventyConfig.addFilter("permalinkDate", (dateObj) => {
        return DateTime.fromJSDate(dateObj, { zone: "utc" }).toFormat(
            // https://github.com/moment/luxon/blob/96994411ae941ce4f2c6aeff55d6f2ac9c21d908/docs/formatting.md#table-of-tokens
            "yyyy-LL-dd"
        );
    });

    eleventyConfig.addFilter("readableDate", (dateObj) => {
        return DateTime.fromJSDate(dateObj, { zone: "utc" }).toFormat(
            "dd LLL yyyy"
        );
    });

    // https://html.spec.whatwg.org/multipage/common-microsyntaxes.html#valid-date-string
    eleventyConfig.addFilter("htmlDateString", (dateObj) => {
        return DateTime.fromJSDate(dateObj, { zone: "utc" }).toFormat(
            "yyyy-LL-dd"
        );
    });

    // Get the first `n` elements of a collection.
    eleventyConfig.addFilter("head", (array, n) => {
        if (!Array.isArray(array) || array.length === 0) {
            return [];
        }
        if (n < 0) {
            return array.slice(n);
        }

        return array.slice(0, n);
    });

    // Return the smallest number argument
    eleventyConfig.addFilter("min", (...numbers) => {
        return Math.min.apply(null, numbers);
    });

    function filterTagList(tags) {
        return (tags || []).filter(
            (tag) => ["all", "nav", "post", "posts"].indexOf(tag) === -1
        );
    }

    eleventyConfig.addFilter("filterTagList", filterTagList);

    // Create an array of all tags
    eleventyConfig.addCollection("tagList", function (collection) {
        let tagSet = new Set();
        collection.getAll().forEach((item) => {
            (item.data.tags || []).forEach((tag) => tagSet.add(tag));
        });

        return filterTagList([...tagSet]);
    });

    // Customize Markdown library and settings:
    let markdownLibrary = markdownIt({
        html: true,
        linkify: true,
    })
        .use(markdownItAnchor, {
            // header anchor plugin
            permalink: markdownItAnchor.permalink.ariaHidden({
                placement: "after",
                class: "direct-link",
                symbol: "#",
            }),
            level: [1, 2, 3, 4],
            slugify: eleventyConfig.getFilter("slugify"),
        })
        .use(markdownItAttrs) // manual html attrs
        .use(markdownItFootnote) // footnote plugin
        .use(
            markdownItForInline,
            "url_new_win",
            "link_open",
            function (tokens, idx) {
                // target blank for links
                const [, /*attrName*/ href] = tokens[idx].attrs.find(
                    (attr) => attr[0] === "href"
                );

                if (href && !href.startsWith("/") && !href.startsWith("#")) {
                    tokens[idx].attrPush(["target", "_blank"]);
                    tokens[idx].attrPush([
                        "rel",
                        "noopener noreferrer external",
                    ]);
                }
            }
        );

    // fit footnotes header in like a section
    // src: https://github.com/markdown-it/markdown-it-footnote
    markdownLibrary.renderer.rules.footnote_block_open = () =>
        '<section class="footnotes">\n' +
        '<h2 class="mt-3" id="footnotes" tabindex="-1">Footnotes <a class="direct-link" href="#footnotes" aria-hidden="true">#</a></h2>\n' +
        '<ol class="footnotes-list">\n';

    eleventyConfig.setLibrary("md", markdownLibrary);

    // Override Browsersync defaults (used only with --serve)
    eleventyConfig.setBrowserSyncConfig({
        callbacks: {
            ready: function (err, browserSync) {
                const content_404 = fs.readFileSync("_site/404.html");

                browserSync.addMiddleware("*", (req, res) => {
                    // Provides the 404 content without redirect.
                    res.writeHead(404, {
                        "Content-Type": "text/html; charset=UTF-8",
                    });
                    res.write(content_404);
                    res.end();
                });
            },
        },
        ui: false,
        ghostMode: false,
    });

    return {
        // Control which files Eleventy will process
        // e.g.: *.md, *.njk, *.html, *.liquid
        templateFormats: ["md", "njk", "html"],

        // Pre-process *.md files with: (default: `liquid`)
        markdownTemplateEngine: "njk",

        // Pre-process *.html files with: (default: `liquid`)
        htmlTemplateEngine: "njk",

        // -----------------------------------------------------------------
        // If your site deploys to a subdirectory, change `pathPrefix`.
        // Don’t worry about leading and trailing slashes, we normalize these.

        // If you don’t have a subdirectory, use "" or "/" (they do the same thing)
        // This is only used for link URLs (it does not affect your file structure)
        // Best paired with the `url` filter: https://www.11ty.dev/docs/filters/url/

        // You can also pass this in on the command line using `--pathprefix`

        // Optional (default is shown)
        pathPrefix,
        // -----------------------------------------------------------------

        // These are all optional (defaults are shown):
        dir: {
            input: ".",
            includes: "_includes",
            data: "_data",
            output: "_site",
        },
    };
};
