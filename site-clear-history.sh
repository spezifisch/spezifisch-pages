#!/bin/bash -e
# this script deletes the _site repository's history by force pushing an orphan branch to main.
# it's probably useless.

[ -d ./_site ] || exit 1
# based on: https://stackoverflow.com/a/13102849

pushd ./_site > /dev/null
git branch -D tmp || true
git checkout -b tmp
git branch -D main
git checkout --orphan main
git branch -D tmp
git add -A
git commit -am publish
git push -f origin main
git gc --aggressive --prune=all
popd > /dev/null
