# spezifisch's personal pages

Source repository for <https://spezifisch.codeberg.page/>.

## My Getting Started Notes

Clone this repository:

```shell
git clone --recurse-submodules git@codeberg.org:spezifisch/spezifisch-pages.git
```

In case you forgot to use `--recurse-submodules`:

```shell
git submodule init
git submodule update
```

### 1. Install dependencies

```shell
pnpm install
cargo install spezilinter
```

### 2. Change stuff

#### Implementation Notes

- `about/index.md` shows how to add a content page.
- `posts/` has the blog posts but really they can live in any directory. They need only the `post` tag to be added to this collection.
- Use the `eleventyNavigation` key in your front matter to add a template to the top level site navigation. For example, this is in use on `index.njk` and `about/index.md`.
- Content can be any template format (blog posts needn’t be markdown, for example). Configure your supported templates in `.eleventy.js` -> `templateFormats`.
- The `css` and `img` directories in the input directory will be copied to the output folder (via `addPassthroughCopy()` in the `.eleventy.js` file).
- The blog post feed template is in `feed/feed.njk`. This is also a good example of using a global data files in that it uses `_data/metadata.json`.
- This example uses three layouts:
  - `_includes/layouts/base.njk`: the top level HTML structure
  - `_includes/layouts/home.njk`: the home page template (wrapped into `base.njk`)
  - `_includes/layouts/post.njk`: the blog post template (wrapped into `base.njk`)
- `_includes/postlist.njk` is a Nunjucks include and is a reusable component used to display a list of all the posts. `index.njk` has an example of how to use it.
- `_data/metadata.json` contains stuff you need to change when using this to bootstrap your own site and publishing it at another URL.

#### Commands

Build and host locally for local development

```shell
pnpm serve
```

Or build automatically when a template changes:

```shell
pnpm watch
```

Or in debug mode:

```shell
pnpm debug
```

### 3. Publish

Generate site and publish to remote git repository:

```shell
pnpm run publish
```

And commit your source changes, too.

## License/Credits

Everything outside the `posts` and `about` directories is MIT licensed, see [./LICENSE](LICENSE) .

See also [./about/index.md](about/index.md#credits) for the credits section as it appears on the resulting site.