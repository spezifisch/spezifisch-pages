#!/bin/bash -e
# this script commits and pushes the _site submodule.
# don't run this script directly! use `pnpm run publish`.

[ -d ./_site ] || exit 1

CID="$(git rev-parse --short HEAD)"

# run image metadata stripping before committing
./.husky/pre-commit || exit 2

pnpm build

pushd ./_site > /dev/null

git add -A
git commit -am "publish build from $CID"

# some _site commits from other machines might get lost but that shouldn't matter because it's generated data anyway.
git push -f origin main

popd > /dev/null
